-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 03, 2019 at 07:08 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

DROP TABLE IF EXISTS `applications`;
CREATE TABLE IF NOT EXISTS `applications` (
  `apid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `prid` int(11) NOT NULL,
  `sname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion_denomination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_spoken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_of_regent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serialno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Processing','Qualified','Approved','Endorsed','UnQualified') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`apid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`apid`, `uid`, `prid`, `sname`, `fname`, `oname`, `dob`, `gender`, `religion_denomination`, `nationality`, `region`, `address`, `phone`, `email`, `language_spoken`, `hear_of_regent`, `image`, `comment`, `file`, `serialno`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Jonah', 'Rimamchirika', 'Boyi', '1993-10-30', 'male', NULL, 'Nigeria', NULL, 'Askoro', NULL, NULL, NULL, 'Advertisement', 'uploadshttp://localhost:8000/img/default-image.png', 'please help me process it fast.', NULL, 'A19-alACa', 'Approved', '2019-04-29 10:48:21', '2019-05-15 18:03:04', NULL),
(2, 1, 2, 'Paul', 'Nathan', 'Isa', '2000-03-01', 'male', NULL, 'Ghana', NULL, 'Kasoa Road', '0238855067', NULL, NULL, 'Friends', 'uploadshttp://localhost:8000/img/default-image.png', 'my dream school.', NULL, 'A19-rsZwh', 'Endorsed', '2019-04-30 08:15:05', '2019-05-02 09:04:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `application_histories`
--

DROP TABLE IF EXISTS `application_histories`;
CREATE TABLE IF NOT EXISTS `application_histories` (
  `ahid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `prid` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion_denomination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_spoken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_of_regent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serialno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `apid` int(11) DEFAULT NULL,
  PRIMARY KEY (`ahid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `application_histories`
--

INSERT INTO `application_histories` (`ahid`, `uid`, `prid`, `name`, `dob`, `gender`, `religion_denomination`, `nationality`, `region`, `address`, `phone`, `email`, `language_spoken`, `hear_of_regent`, `image`, `comment`, `serialno`, `status`, `created_at`, `updated_at`, `deleted_at`, `apid`) VALUES
(1, 1, NULL, 'RIMAMCHIRIKA JONAH BOYI', '1993-10-30', 'male', NULL, 'Nigeria', NULL, 'Askoro', NULL, NULL, NULL, 'Advertisement', 'uploadshttp://localhost:8000/img/default-image.png', 'please help me process it fast.', 'A19-alACa', 'Approved', '2019-04-30 22:10:15', '2019-04-30 22:10:15', NULL, 1),
(2, 1, NULL, 'RIMAMCHIRIKA JONAH BOYI', '1993-10-30', 'male', NULL, 'Nigeria', NULL, 'Askoro', NULL, NULL, NULL, 'Advertisement', 'uploadshttp://localhost:8000/img/default-image.png', 'please help me process it fast.', 'A19-alACa', 'Endorsed', '2019-04-30 22:41:41', '2019-04-30 22:41:41', NULL, 1),
(3, 1, 2, 'NATHAN PAUL ISA', '2000-03-01', 'male', NULL, 'Ghana', NULL, 'Kasoa Road', '0238855067', NULL, NULL, 'Friends', 'uploadshttp://localhost:8000/img/default-image.png', 'my dream school.', 'A19-rsZwh', 'Endorsed', '2019-05-02 09:04:02', '2019-05-02 09:04:02', NULL, 2),
(4, 1, 1, 'RIMAMCHIRIKA JONAH BOYI', '1993-10-30', 'male', NULL, 'Nigeria', NULL, 'Askoro', NULL, NULL, NULL, 'Advertisement', 'uploadshttp://localhost:8000/img/default-image.png', 'please help me process it fast.', 'A19-alACa', 'Qualified', '2019-05-15 18:01:19', '2019-05-15 18:01:19', NULL, 1),
(5, 1, 1, 'RIMAMCHIRIKA JONAH BOYI', '1993-10-30', 'male', NULL, 'Nigeria', NULL, 'Askoro', NULL, NULL, NULL, 'Advertisement', 'uploadshttp://localhost:8000/img/default-image.png', 'please help me process it fast.', 'A19-alACa', 'Approved', '2019-05-15 18:03:04', '2019-05-15 18:03:04', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `fid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `gid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `caption` varchar(7000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
CREATE TABLE IF NOT EXISTS `gallery_images` (
  `giid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`giid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_04_26_134614_admission', 1),
(5, '2019_04_29_075724_add_to_admissions_table', 2),
(6, '2019_04_29_114158_adtable', 3),
(7, '2019_04_30_105110_appointmenthistory', 4),
(8, '2019_04_30_214347_add_to_applications_table', 5),
(9, '2019_05_11_205126_staff', 6),
(10, '2019_05_14_181500_add_to_users_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
CREATE TABLE IF NOT EXISTS `programs` (
  `prid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`prid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`prid`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Information System Science', 'A sub division of computer science', '2019-04-29 08:13:28', '2019-04-29 08:13:28', NULL),
(3, 'Computer Engineering', 'Computing aspect of engineering', '2019-04-29 09:24:10', '2019-05-10 18:06:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `stid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('Applicant','Admin','VPAcademics','Registrar','AdmissionsOffice') COLLATE utf8mb4_unicode_ci DEFAULT 'Applicant',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`stid`),
  UNIQUE KEY `staff_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`stid`, `title`, `name`, `gender`, `phone`, `image`, `email`, `password`, `role`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Mr', 'Dayu Alex', 'Male', '0238855069', NULL, 'dayualex@gmail.com', '$2y$10$aEXx3yiRFMZcr/p1tusqTOUDocLPUxKQeMxDnNpKjkV5Lw03I4r2W', 'AdmissionsOffice', '2019-05-12 06:24:53', '2019-05-12 06:24:53', NULL),
(2, 'Mr', 'Attah Opoku', 'Male', '0230000044', NULL, 'attahopoku@regent.edu.gh', '$2y$10$5/RKj5xZq62K4kxv.MO4vuRdYCrpA5WRdbYI4BQYRAExPla3hNkXG', 'VPAcademics', '2019-06-03 18:02:25', '2019-06-03 18:02:25', NULL),
(3, 'Mr', 'Frank A', 'Gender', '0238598675', NULL, 'frank@regent.edu.gh', '$2y$10$eerWj0i08w3XhyaoApF94eoQ6puyEl4r7.bfvmjOT70cL3Sl4ablO', 'AdmissionsOffice', '2019-06-03 18:05:48', '2019-06-03 18:05:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `uid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `apid` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Applicant','Admin','VPAcademics','Registrar','AdmissionsOffice') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Applicant',
  `state` enum('active','in_active') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `apid`, `name`, `email`, `email_verified_at`, `password`, `role`, `state`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Solomon Osei', 'solomonosei@gmail.com', NULL, '$2y$10$wYtqRvxNeG4kyn/PSDuh6OlrBrOw2msX5PE1M8eZAP.9tmbpK/y1u', 'Admin', 'active', NULL, '2019-04-28 09:55:58', '2019-05-15 17:58:39'),
(2, 2, 'Dayu Alex', 'dayualex@gmail.com', NULL, '$2y$10$aEXx3yiRFMZcr/p1tusqTOUDocLPUxKQeMxDnNpKjkV5Lw03I4r2W', 'Applicant', 'active', NULL, '2019-05-12 06:24:53', '2019-05-15 17:23:39'),
(3, NULL, 'Elisha Yaw', 'elishayaw@gmail.com', NULL, '$2y$10$20TUH.t6nOAN7GsExX3CpufaiY6bM953Cw9/5l2PV0KOYQ4mAYY.K', 'Registrar', 'active', NULL, '2019-05-16 06:03:38', '2019-05-16 06:03:38'),
(4, NULL, 'Attah Opoku', 'attahopoku@regent.edu.gh', NULL, '$2y$10$5/RKj5xZq62K4kxv.MO4vuRdYCrpA5WRdbYI4BQYRAExPla3hNkXG', 'VPAcademics', 'active', NULL, '2019-06-03 18:02:25', '2019-06-03 18:02:25'),
(5, NULL, 'Frank A', 'frank@regent.edu.gh', NULL, '$2y$10$eerWj0i08w3XhyaoApF94eoQ6puyEl4r7.bfvmjOT70cL3Sl4ablO', 'AdmissionsOffice', 'active', NULL, '2019-06-03 18:05:48', '2019-06-03 18:05:48');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
