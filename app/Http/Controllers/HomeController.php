<?php

namespace App\Http\Controllers;


use App\application;

use App\program;
use App\User;
use App\staff;
use App\applicationhistory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\App;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $applications = application::orderBy('created_at','desc')->paginate(5);
        $program = program::all();
        $qualifiedapplications = application::where('status','Qualified')->count();
        $approvedapplications = application::where('status','Approved')->count();
        $endorsedapplications = application::where('status','Endorse')->count();
        $unqualifiedapplications = application::where('status','UnQualified')->count();


        return view('mainhome', [
            'applications' => $applications,
            'program' => $program,
            'qualifiedapplications' => $qualifiedapplications,
            'endorsedapplications' => $endorsedapplications,
            'unqualifiedapplications' => $unqualifiedapplications


        ]);
        
    }

    public function applicantProfile(){

        $applications = application::all();


    	return view('applicantProfile',[
            'applications' => $applications

	    ]);

    }

    public function addProgram(){

        return view('backend.programs.add');
    }

    public function postAddProgram(Request $request){
        $program = new program();
		$program->name = $request->input('name');
		$program->description = $request->input('description');
		// $program->uid = Auth::user('admin')->uid;
        $status = $program->save();
        if($status){
            $request->session()->flash('success','Program successful created!!');

        }else{
            $request->session()->flash('error','Oops Something went wrong!');
   
        }
        return redirect()->back();

    }

    public function manageProgram(){
        $programs = program::orderBy('created_at','desc')->paginate(10);
		return view('backend.programs.manage', [
			'programs' => $programs
		]);

    }

    public function editProgram( $prid ) {
		$program = program::find($prid);

		return view('backend.programs.edit',[
			'program' => $program
		]);
    }
    
    public function postEditProgram(Request $request, $prid){

        $program  =  program::findorfail($prid);
        $status = $program->update($request->all());
        
        if($status)
            
            return redirect('/manage-program')->with('success' , 'Program Record Updated successfully');
		else
		    
        return redirect('/manage-program')->with('error' , 'Sorry something went wrong contact IT');
    }

    
    public function deleteProgram ($prid){
        $program = program::destroy($prid);
        if ($program){
            session()->flash('success','Program Record Deleted successfully');
        }else{
            session()->flash('error','Sorry something went wrong');

        }
        return redirect()->back();
    }
    public function getApplicantInfo(){

    }

    public function postQualifiedApplicant(Request $request, $apid){

        try{

			DB::beginTransaction();

				$application = application::findorfail($apid);
				$application->status='Qualified';
				$application->save();

				$a_fname = str_slug($application->fname);
                $a_sname = str_slug($application->sname);
                $a_oname = str_slug($application->oname);   
        		$fullname = strtoupper($a_fname . " " . $a_sname. " ". $a_oname);

                $applicationhistory                        = new applicationhistory();
                $applicationhistory->apid                  = $application->apid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->prid                  = $application->prid;
                $applicationhistory->uid                   = $application->uid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->dob                   = $application->dob;
                $applicationhistory->gender                = $application->gender;
                $applicationhistory->nationality           = $application->nationality;
                $applicationhistory->religion_denomination = $application->religion_denomination;
                $applicationhistory->address               = $application->address;
                $applicationhistory->phone                 = $application->phone;
                $applicationhistory->language_spoken       = $application->language_spoken;
                $applicationhistory->region                = $application->region;
                
                
                $applicationhistory->email                 = $application->email;
                $applicationhistory->hear_of_regent        = $application->hear_of_regent;
                $applicationhistory->image                 = $application->image;
        
                $applicationhistory->comment               = $application->comment;
                $applicationhistory->status                = $application->status;
                $applicationhistory->serialno              = $application->serialno;
                $applicationhistory->save();
                
            
                DB::commit();


            session()->flash('success','Candidate qualified');
            return redirect()->back();
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
			return redirect()->back();
        }

    }

    public function approveApplicant(Request $request, $apid){

        try{

			DB::beginTransaction();

				$application = application::findorfail($apid);
				$application->status='Approved';
				$application->save();

				$a_fname = str_slug($application->fname);
                $a_sname = str_slug($application->sname);
                $a_oname = str_slug($application->oname);   
        		$fullname = strtoupper($a_fname . " " . $a_sname. " ". $a_oname);

				$applicationhistory                        = new applicationhistory();
                $applicationhistory->apid                  = $application->apid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->prid                  = $application->prid;
                $applicationhistory->uid                   = $application->uid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->dob                   = $application->dob;
                $applicationhistory->gender                = $application->gender;
                $applicationhistory->nationality           = $application->nationality;
                $applicationhistory->religion_denomination = $application->religion_denomination;
                $applicationhistory->address               = $application->address;
                $applicationhistory->phone                 = $application->phone;
                $applicationhistory->language_spoken       = $application->language_spoken;
                $applicationhistory->region                = $application->region;
                
                
                $applicationhistory->email                 = $application->email;
                $applicationhistory->hear_of_regent        = $application->hear_of_regent;
                $applicationhistory->image                 = $application->image;
        
                $applicationhistory->comment               = $application->comment;
                $applicationhistory->status                = $application->status;
                $applicationhistory->serialno              = $application->serialno;
                $applicationhistory->save();
                        
        
                
			DB::commit();


            session()->flash('success','Candidate qualified');
            return redirect()->back();
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
			return redirect()->back();
        }
    }

    
    public function endorseApplicant(Request $request, $apid){
        try{

			DB::beginTransaction();

				$application = application::findorfail($apid);
				$application->status='Endorsed';
				$application->save();

				$a_fname = str_slug($application->fname);
                $a_sname = str_slug($application->sname);
                $a_oname = str_slug($application->oname);   
        		$fullname = strtoupper($a_fname . " " . $a_sname. " ". $a_oname);

				$applicationhistory                        = new applicationhistory();
                $applicationhistory->apid                  = $application->apid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->prid                  = $application->prid;
                $applicationhistory->uid                   = $application->uid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->dob                   = $application->dob;
                $applicationhistory->gender                = $application->gender;
                $applicationhistory->nationality           = $application->nationality;
                $applicationhistory->religion_denomination = $application->religion_denomination;
                $applicationhistory->address               = $application->address;
                $applicationhistory->phone                 = $application->phone;
                $applicationhistory->language_spoken       = $application->language_spoken;
                $applicationhistory->region                = $application->region;
                
                
                $applicationhistory->email                 = $application->email;
                $applicationhistory->hear_of_regent        = $application->hear_of_regent;
                $applicationhistory->image                 = $application->image;
        
                $applicationhistory->comment               = $application->comment;
                $applicationhistory->status                = $application->status;
                $applicationhistory->serialno              = $application->serialno;
                $applicationhistory->save();
                        
                        
			DB::commit();

            session()->flash('success','Candidate qualified');
            return redirect()->back();
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
			return redirect()->back();
        }
    }


    public function unqualifyApplicant(Request $request, $apid){

        try{

			DB::beginTransaction();

				$application = application::findorfail($apid);
				$application->status='UnQualified';
				$application->save();

				$a_fname = str_slug($application->fname);
                $a_sname = str_slug($application->sname);
                $a_oname = str_slug($application->oname);   
        		$fullname = strtoupper($a_fname . " " . $a_sname. " ". $a_oname);

				$applicationhistory                        = new applicationhistory();
                $applicationhistory->apid                  = $application->apid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->prid                  = $request->prid;
                $applicationhistory->uid                   = $application->uid;
                $applicationhistory->name                  = $fullname;
                $applicationhistory->dob                   = $application->dob;
                $applicationhistory->gender                = $application->gender;
                $applicationhistory->nationality           = $application->nationality;
                $applicationhistory->religion_denomination = $application->religion_denomination;
                $applicationhistory->address               = $application->address;
                $applicationhistory->phone                 = $application->phone;
                $applicationhistory->language_spoken       = $application->language_spoken;
                $applicationhistory->region                = $application->region;
                
                
                $applicationhistory->email                 = $application->email;
                $applicationhistory->hear_of_regent        = $application->hear_of_regent;
                $applicationhistory->image                 = $application->image;
        
                $applicationhistory->comment               = $application->comment;
                $applicationhistory->status                = $application->status;
                $applicationhistory->serialno              = $application->serialno;
                $applicationhistory->save();
                        
			DB::commit();

            session()->flash('success','Candidate qualified');
            return redirect()->back();
        }
        catch(\Exception $exception){
            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact Technical Staff.");
            
			return redirect()->back();
        }
    }
    
	
    
	public function manageApplications(){
        
        $applications = application::orderBy('created_at','desc')->paginate(10);
        $programs = program::all();
		return view('backend.applications.manage', [
            'applications' => $applications,
            '$programs' => $programs
		]);

    }
    
    public function getNewApplications(){
        
        $applications = application::where('status','Processing')->paginate(10);
        $programs = program::all();
		return view('backend.applications.qualify', [
            'applications' => $applications,
            '$programs' => $programs
		]);

	}

    public function getQualifiedApplications(){
        $applications = application::where('status','Qualified')->paginate(10);
        $programs = program::all();
		return view('backend.applications.approve', [
            'applications' => $applications,
            '$programs' => $programs
		]);
    }

    public function getApprovedApplications(){
        $applications = application::where('status','Approved')->orWhere('status', 'Endorsed')->paginate(10);
        $programs = program::all();
		return view('backend.applications.endorse', [
            'applications' => $applications,
            '$programs' => $programs
		]);
    }

    public function getUnqualifiedApplicants(){
        $applications = application::where('status','UnQualified')->paginate(10);
        $programs = program::all();
		return view('backend.applications.unqualify', [
            'applications' => $applications,
            '$programs' => $programs
		]);
    }





    public function getAddStaff(){
        return view('backend.staff.add');
    }


    public function manageStaff(){
        $staff = user::orderBy('created_at','desc')->paginate(5);
        return view('backend.staff.manage', [
            'staff' => $staff
            ]);
        
        return view('backend.staff.manage');

    }

    public function editStaff($uid){
        $staff = staff::findorfail($uid);
		return view('backend.staff.edit',[
            'staff' => $staff
    		]);
    }

    public function postEditStaff(Request $request, $stid){
        
        $staff  =  staff::findorfail($uid);
		$status = $staff->update($request->all());
	
        if($request->hasFile('image')){
            $destinatonPath = '';
            $filename = '';
    
            $file = Input::file('image');
            $destinationPath = public_path().'staff/images/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
        }else {
            $filename = url('default-image.png');
        }



		if($status)
            return redirect('/manage-staff')->with('success' , 'Staff Record Edited successfully');
		else

        return redirect('/manage-staff')->with('error','Sorry an error occurred');
    }

    public function deleteStaff($uid){
        
        $staff = staff::destroy($uid);
        if ($staff){
            session()->flash('success','Staff Record Deleted successfully');
        }else{
            session()->flash('error','Sorry something went wrong');

        }
        return redirect()->back();
    }



    public function viewStaffDetails($uid){

     
        $staff = staff::findorfail($uid);
        return view ('backend.staff.viewStaffDetails',[
            'staff' => $staff
        ]);
    }

    public function postAddStaff (Request $request) {
        
        try{

        DB::beginTransaction();
        
        if($request->hasFile('image')){
            $destinatonPath = '';
            $filename = '';
    
            $file = Input::file('image');
            $destinationPath = public_path().'staff/images/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
        }else {
            $filename = url('/images/default-image.png');

        }

        //  This is  Partially required
        $staff = new staff();
		$staff->title = $request->input('title');
		$staff->name = $request->input('name');
		$staff->gender = $request->input('gender');
		$staff->phone = $request->input('phone');
		$staff->image = $request->input('image');
		$staff->email = strtolower($request->input('email'));
		$staff->password = bcrypt("Regent123");    
        $staff->role = $request->input('role');
        $staff->save();

       

        $user      = new User();
        // $user->stid = $staff->stid;
        $user->name = $staff->name;
        $user->email = $staff->email;
        $user->password = $staff->password;
        $user->role = $staff->role;
        $user->state = 'active';
		$user->save();
        
        DB::commit();
		
            return redirect('/manage-staff')->with('success' , 'Staff Added successfully');
            
        }catch (\Exception $exception){


            return $exception->getMessage();
            session()->flash('error',"Something went wrong. Please try again or contact IT.");

            return redirect()->back();
        }


    }
    

    public function changePassword() {
		return view('changePassword');
	}

	public function postChangePassword( Request $request ) {
		$oldpassword = $request->input('oldpassword');
		$password = $request->input('password');
		$confirmpassword = $request->input('confirmpassword');
		$user = User::find(Auth::user()->uid);

		if(password_verify($oldpassword,$user->password) ){

			if($confirmpassword == $password){
				$user->password = bcrypt($password);
				$request->session()->flash("success","Password changed successfully");
				$user->save();
				return redirect('/home');

			}
			else{
				$request->session()->flash("error", "Both new passwords don't match. Please try again");
				return redirect('/change-password');
			}

		} else {
			$request->session()->flash("error", "Current Password is wrong. Please try again.");
			return redirect('/change-password');
		}
        return redirect('/home')->with('Success', 'Password Changed Successfully');
    }
    
   

   
    public function profile(){
        
        return view ('profile');
    }

    
    public function makeApplicant( Request $request,$uid) {
		$user = User::findorfail($uid);
		$user->role = 'Applicant';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Applicant');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
    }
    
    public function makeVPAcademic( Request $request,$uid) {
		$user = User::findorfail($uid);
		$user->role = 'VPAcademics';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Vice President Academic');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
    }

    public function makeRegistrar( Request $request,$uid) {
		$user = User::findorfail($uid);
		$user->role = 'Registrar';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Registrar');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
    }
    
    public function makeAdmissionOfficer( Request $request,$uid) {
		$user = User::findorfail($uid);
		$user->role = 'AdmissionsOffice';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Admission Officer');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
    }
    

    public function makeAdmin( Request $request, $uid) {
		$user = User::findorfail($uid);
		$user->role = 'Applicant';
		$status =  $user->save();
		if ($status){
			session()->flash('success','Role Changed to Applicant');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
    }
    
    public function generateLetter($apid){
        $application = application::findorfail($apid);
		$program = program::all();
        return view ('backend.applications.generateLetter',[
            'application' => $application,
            'program' => $program
        ]);
    }

    public function getExport(){
        return view ('backend.export');
    }

    public function logout() {
		auth()->logout();
		return redirect('/');
    }
    
    // Note: both get and post method in the route file can make a post request to the database.
    // Todo: Rearrange all the routes, Report, Genarate/Upload, Testing, counting in real time 
}
