<?php

namespace App\Http\Controllers;

use App\application;
use App\program;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;
use Illuminate\Support\Str;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Support\Facades\App;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class AdmissionController extends Controller
{
    //

    public function __construct()
	{
		$this->middleware('auth');
	}

    public function getStartApplication(){
        $program = program::all();
        return view('frontend.start', [
            'program' => $program
        ]);
    }

    public function postApplication(Request $request){


        if($request->hasFile('image')){
			$destinatonPath = '';
			$filename = '';
	
			$file = Input::file('image');
			$destinationPath = public_path().'/uploads/';
			$filename = str_random(6).'_'.$file->getClientOriginalName();
			$uploadSuccess = $file->move($destinationPath, $filename);
		}else {
			$filename = url('img/default-image.png');
		}

        $applicationid = 'A19-'. Str::random(5);

        $application = new Application();
        $application->prid = $request->input('prid');
        $application->uid = Auth::User()->uid;

		$application->sname = $request->input('sname');
		$application->fname = $request->input('fname');
		$application->oname = $request->input('oname');
		$application->dob = $request->input('dob');
		$application->gender = $request->input('gender');
		$application->nationality = $request->input('nationality');
		$application->religion_denomination = $request->input('religion_denomination');
		$application->address = $request->input('address');
		$application->phone = $request->input('phone');
		$application->language_spoken = $request->input('language_spoken');
		$application->region = $request->input('region');
		
		
		$application->email = strtolower($request->input('email'));
		$application->hear_of_regent = $request->input('hear_of_regent');
		$application->image = 'uploads'. $filename;

		$application->comment = strtolower($request->input('comment'));
		$application->status = 'Processing';
        $application->serialno = $applicationid;
		$status = $application->save();

		if ($status){
			// session()->flash('success','Application Created Sucessfully.');
			return redirect('/pay')->with('success','Application Created Sucessfully.');
		}else{
			session()->flash('error','Something went Wrong, Try again');
			return redirect()->back();
		}

	}
	

	public function getPay(){
		return view('frontend.pay');
	}

}
