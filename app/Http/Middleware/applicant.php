<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class applicant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        if(Auth::user()->role == 'Applicant'){
		    return $next($request);
	    } else {
            $request->session()->flash('error','Sorry you are not permitted to access that route.');
		    return redirect('/home')->with('error','Sorry you are not permitted to access that route.');
	    }
    }
}
