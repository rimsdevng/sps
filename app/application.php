<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class application extends Model
{
    //
    protected $primaryKey = 'apid';
    protected $table = 'applications';

    
    protected $fillable = [];

    public function Program() {
		return $this->belongsTo(program::class,'prid', 'prid');
   }
   
   public function User() {
		return $this->belongsTo(User::class, 'uid');
	 }
}
