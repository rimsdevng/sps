<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class program extends Model
{
    //
    protected $primaryKey = 'prid';
    
    protected $guarded = [];
    
    // protected $fillable = [];
    protected $table = 'programs';

    public function Application(  ) {
    // return $this->hasMany(application::class,'prid','prid');         -> return an array and not an object
    
      return $this->belongsTo('App\application', 'prid');
    }
}
