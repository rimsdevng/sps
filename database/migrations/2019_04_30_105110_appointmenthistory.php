<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Appointmenthistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('application_histories', function (Blueprint $table){

		    //details of applicant
            $table->increments('ahid');
            $table->integer('apid')->nullable();
            $table->integer('uid')->nullable();
            $table->integer('prid')->nullable();
		    $table->string('name')->nullable();
		    $table->string('dob')->nullable();
		    $table->enum('gender',['male','female'])->nullable();
		    $table->string('religion_denomination')->nullable();
            $table->string('nationality')->nullable();
            $table->string('region')->nullable();
		    $table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
		    $table->string('language_spoken')->nullable();
		    
		    //hear about us
		    $table->string('hear_of_regent')->nullable();
		    //others
            $table->string('image')->nullable();
			$table->string('comment')->nullable();
						
		    //auto Generated
            $table->string('serialno')->nullable();
            $table->string('status')->nullable();
            

		    $table->timestamps();
		    $table->softDeletes();

        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
