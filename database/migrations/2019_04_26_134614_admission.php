<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Admission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        
	    Schema::create('applications', function (Blueprint $table){

		    //details of applicant
		    $table->increments('apid');
            $table->integer('uid');
            $table->integer('prid');
		    $table->string('sname')->nullable();
		    $table->string('fname')->nullable();
		    $table->string('oname')->nullable();
		    $table->string('dob')->nullable();
		    $table->enum('gender',['male','female'])->nullable();
		    $table->string('religion_denomination')->nullable();
            $table->string('nationality')->nullable();
            $table->string('region')->nullable();
		    $table->string('address')->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
		    $table->string('language_spoken')->nullable();
		    
		    //hear about us
		    $table->string('hear_of_regent')->nullable();
		    //others
            $table->string('image')->nullable();
			$table->string('comment')->nullable();
			
            $table->string('file')->nullable();
			
		    //auto Generated
            $table->string('serialno')->nullable();
            $table->enum('status',['Processing','Qualified','Approved','Endorse','UnQualified'])->nullable();
            

		    $table->timestamps();
		    $table->softDeletes();

	    });



	    Schema::create('faqs', function(Blueprint $table){
		    $table->increments('fid');
		    $table->string('question');
		    $table->string('answer',5000);
		    $table->timestamps();
		    $table->softDeletes();
        });
        
        Schema::create('programs', function(Blueprint $table){
		    $table->increments('prid');
		    $table->string('name');
		    $table->string('description',5000);
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('gallery', function(Blueprint $table){
		    $table->increments('gid');
		    $table->string('name');
		    $table->string('cover');
		    $table->string('caption',7000);
		    $table->timestamps();
		    $table->softDeletes();
	    });


	    Schema::create('gallery_images', function(Blueprint $table){
		    $table->increments('giid');
		    $table->integer('gid');
		    $table->string('image');
		    $table->timestamps();
		    $table->softDeletes();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
