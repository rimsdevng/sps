<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table){

		    //details of applicant
		    $table->increments('apid');
            $table->integer('uid');
            $table->integer('prid');
		    $table->string('sname')->nullable();
		    $table->string('fname')->nullable();
		    $table->string('oname')->nullable();
		    $table->string('dob')->nullable();
		    $table->enum('gender',['male','female'])->nullable();
		    $table->string('religion_denomination')->nullable();
            $table->string('nationality')->nullable();
            $table->string('region')->nullable();
		    $table->string('address')->nullable();
			$table->string('phone')->nullable();
            
            $table->string('email')->nullable();
		    $table->string('language_spoken')->nullable();
		    
		    //hear about us
		    $table->string('hear_of_regent')->nullable();
		    //others
            $table->string('image')->nullable();
			$table->string('comment')->nullable();
			
            $table->string('file')->nullable();
			
		    //auto Generated
            $table->string('serialno')->nullable();
            $table->enum('status',['Processing','Qualified','Approved','Endorsed','UnQualified'])->nullable();
            

		    $table->timestamps();
		    $table->softDeletes();

        });
        
        
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('uid');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('role',['Applicant','Admin','VPAcademics','Registrar'])->default('Applicant');
		    $table->enum('state',['active','in_active'])->default('active');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
