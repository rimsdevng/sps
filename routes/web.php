<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout','HomeController@logout');

Route::get('/profile','HomeController@applicantProfile');


Route::get('/program','HomeController@addProgram');
Route::post('/program','HomeController@postAddProgram');
Route::get('/manage-program','HomeController@manageProgram');
Route::get('/edit-program/{prid}', 'HomeController@editProgram');
Route::post('/post-edit-program/{prid}','HomeController@postEditProgram');
Route::get('/delete-program/{prid}', 'HomeController@deleteProgram');

Route::get('add-staff', 'HomeController@getAddStaff');
Route::post('add-staff', 'HomeController@postAddStaff');
Route::get('manage-staff', 'HomeController@manageStaff');
Route::get('edit-staff/{stid}', 'HomeController@editStaff');
Route::post('post-edit-staff/{stid}', 'HomeController@postEditStaff');
Route::get('delete-staff/{stid}', 'HomeController@deleteStaff');

Route::get('view-staff-detail/{stid}', 'HomeController@viewStaffDetails');

Route::get('make-admin/{uid}','HomeController@makeAdmin');
Route::get('make-applicant/{uid}', 'HomeController@makeApplicant');
Route::get('make-vpacademics/{uid}','HomeController@makeVPAcademic');
Route::get('make-admissionOfficer/{uid}','HomeController@makeAdmissionOfficer');
Route::get('make-registrar/{uid}','HomeController@makeRegistrar');

//applications
Route::get('manage-applications','HomeController@manageApplications');

Route::group(['middleware' => 'applicant'], function(){
    //clients public routs
    Route::get('start-application', 'AdmissionController@getStartApplication');
    Route::post('post-application', 'AdmissionController@postApplication');

});

    // get applicant info
Route::get('view-applicant-info/{apid}','HomeController@getApplicantInfo');


Route::group(['middleware' => 'admissionsoffice'], function(){
    // Qualify Applicant
    Route::get('get-applicants','HomeController@getNewApplications');
    Route::get('qualify-new-applicant/{apid}','HomeController@postQualifiedApplicant');
    Route::get('get-unqualify-applicants','HomeController@getUnqualifiedApplicants');
    Route::post('unqualify-applicant/{apid}','HomeController@unqualifyApplicant');

});




Route::group(['middleware' => 'vpacademic'], function(){
    // Approve Applicant
    Route::get('get-qualified-applicants','HomeController@getQualifiedApplications');
    Route::post('approve-applicant/{apid}','HomeController@approveApplicant');
});


Route::group(['middleware' => 'registrar'], function(){
    // Endorse Applicant
    Route::get('get-approved-applicants','HomeController@getApprovedApplications');
    Route::get('endorse-applicant/{apid}','HomeController@endorseApplicant');
    Route::get('generate-letter/{apid}', 'HomeController@generateLetter');
});




Route::get('staff/login','AuthenticationController@getStaffLogin');

Route::get('export', 'HomeController@getExport');
Route::get('pay','AdmissionController@getPay');

// generate letter upload document 


// list of applications 
// reports for qualified or unqualified applicant
// redirect to pay page after submission 