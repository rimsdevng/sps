<?php

namespace App\Http\Controllers;

use App\Application;
use App\Mail\startApplicationMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ApplicationController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth')->except(["continueApplication","start","postStartApplication"]);
		$this->middleware('isApplicant')->except(["continueApplication","start","postStartApplication"]);
	}

	public function start() {
		return view('application.start');
	}


	public function postStartApplication( Request $request ) {
		$name = $request->input('name');
		$email = $request->input('email');

		$user = new User();
		$user->name = $name;
		$user->email = $email;
		$user->password = bcrypt($email);
		$user->save();

		$application = new Application();
		$application->uid = $user->uid;
		$application->save();

		auth()->login($user);
		Mail::to($email)->send(new startApplicationMail($user));
		return redirect('application/get-child-details');
	}


	public function getChildDetails() {
		return view('application.child_detail');
	}


	public function continueApplication() {
		$uid = Input::get('u'); // get email
		$token = Input::get('t'); // get token
		$user = User::find($uid);
		if($user->password == $token){
			auth()->login($user);
			return redirect('application/get-child-details');
		}
	}


	public function postChildDetails(Request $request  ) {

		$save = $request->input('save');

		$application = Application::find(auth()->user()->Application->apid);
		$application->surname = $request->input('surname');
		$application->forenames = $request->input('forenames');
		$application->preferred_name = $request->input('preferred_name');
		$application->dob = $request->input('dob');
		$application->gender = $request->input('gender');
		$application->nationality = $request->input('nationality');
		$application->religion_denomination = $request->input('religion_denomination');
		$application->home_address = $request->input('home_address');
		$application->home_phone = $request->input('home_phone');
		$application->first_language = $request->input('first_language');
		$application->proposed_year = $request->input('proposed_year');
//		$application->connections = $request->input('connection');
//		$application->accommodation = $request->input('accommodation');

		$application->save();

		if(isset($save)){

			Mail::to(auth()->user()->email)->send(new startApplicationMail(auth()->user()));
			session()->flash('success','Application Saved');

		}

		return redirect('application/get-school-details');

	}


	public function getSchoolDetails(  ) {
		return view('application.school');
	}

	public function postSchoolDetails( Request $request ) {



		$school = Application::where('uid',Auth::user()->uid)->first();

		$school->update($request->all());


//		$school = new Application();
//		$school->uid = Auth::user()->uid;
//		$school->current_school = $request->input('current_school');
//		$school->attendance_date = $request->input('attendance_date');
//		$school->head_teacher = $request->input('head_teacher');
//		$school->head_teacher_email = $request->input('head_teacher_email');
//		$school->school_phone = $request->input('school_phone');
//		$status = $school->save();
//
//		if ($status){
//			session()->flash('success','School Information Saved');
//		}else{
//			session()->flash('error','Sorry, there was an error');
//		}



		return redirect('application/get-parent');



	}







	public function getParentDetails(  ) {
		return view('application.parent_details');
	}


	public function postParentDetails(Request $request  ) {


		$parent = Application::where('uid',Auth::user()->uid)->first();

		$parent->update($request->all());


//		$parent = new Application();
//		$parent->father = $request->input('father');
//		$parent->father_title = $request->input('father_title');
//		$parent->father_profession = $request->input('father_profession');
//		$parent->father_industry = $request->input('father_industry');
//		$parent->father_employer_name = $request->input('father_employer_name');
//		$parent->father_email = $request->input('father_email');
//		$parent->father_phone = $request->input('father_phone');
//
//
//		$parent->mother = $request->input('mother');
//		$parent->mother_title = $request->input('mother_title');
//		$parent->mother_profession = $request->input('mother-profession');
//		$parent->mother_industry  = $request->input('mother_industry');
//		$parent->mother_employer_name = $request->input('mother_employer_name');
//		$parent->mpther_email = $request->input('mother_email');
//		$parent->mother_phone = $request->input('mother_phone');
//		$parent->save();

		return redirect('application/get-about-hendon');
	}


	public function getAboutHendon(  ) {
		return view('application.hear_of_hendon');
	}

	public function postAboutHendon(Request $request  ) {

		$about = Application::where('uid',Auth::user()->uid)->first();
		$about->update($request->all());
		return redirect('application/get-others');
	}

	public function getOthers(  ) {
		return view('application.other_people');
	}


	public function postOthers(Request $request  ) {
			$other = Application::where('uid',Auth::user()->uid)->first();
			$other->update($request->all());

//		$others = new Application();
//		$others->fname = $request->input('fname');
//		$others->sname = $request->input('sname');
//		$others->address = $request->input('address');
//		$others->email = $request->input('email');
//		$others->phone = $request->input('phone');
//		$others->relationship_to_child = $request->input('relationship_to_child');
//
//		$others->save();

		return redirect('application/get-declaration');

	}

	public function getDeclaration(  ) {
		return view('application.declaration');
	}

	public function postDeclaration( Request $request ) {
		$declear = Application::where('uid',Auth::user()->uid)->first();
		$declear->update($request->all());
		return redirect('application/done');
	}


	public function done(  ) {
		return view('application.done');
	}


}

