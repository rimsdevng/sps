@extends('layouts.front')


@section('content')

<!--Header-->
        <div class="header">
        <div class="top">
                        <div class="container">
                            
                                <div class="col-md-9 top-left">
                                    <ul>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i> Jonkobri Rd, Accra</li>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i> +(233) 223 886 6067</li>
                                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">regentads@regent.edu.com</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3 top-middle">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                                
                                <div class="clearfix"></div>
                            
                        </div>
                    </div>
            <!--top-bar-w3layouts-->
            @include('navs.mainNav')
            <!--//top-bar-w3layouts-->
            <!--Slider-->
            @include('navs.fslider')
            <!--//Slider-->
        </div>
        <!--//Header-->

        <!-- services -->
        <div class="w3-agile-services">
            <div class="container">
                <h3 class="title-txt" id="about"><span>O</span>verviews</h3>
            <div class="agileits-services">
                    <div class="services-right-grids">
                        <div class="col-sm-4 services-right-grid">
                            <div class="se-top">
                                <div class="services-icon">
                                    <i class="fa fa-book" aria-hidden="true"></i>
                                </div>
                                <div class="services-icon-info">
                                    <h5>Regent, Agenda</h5>
                                    <p>Regent University College of Science and Technology is an institution with a national agenda and a global focus.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 services-right-grid">
                            <div class="se-top">
                                <div class="services-icon">
                                <i class="fa fa-eye" aria-hidden="true"></i>
                                </div>
                                <div class="services-icon-info">
                                    <h5>Vision</h5>
                                    <p>To become a centre of excellence in raising highly skillful, visionary, God-fearing, critical thinkers, ethical and passionate leaders, to function as change-agents in their various spheres of life.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 services-right-grid">
                            <div class="se-top">
                                <div class="services-icon">
                                <i class="fa fa-flask" aria-hidden="true"></i>
                                </div>
                                <div class="services-icon-info">
                                    <h5>PRACTICAL SCHOOL</h5>
                                    <p>University gives more than a degree and a certificate; it provides more than a job to employees. Regent empowers individual to become people of influence, to impact generations.</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- //services -->
    
        <!--about-->
            {{-- <div class="about">
                <div class="container">
                    <div class="about-main">
                        <div class="about-right">
                            <h3 class="subheading-w3-agile">About Us</h3>
                            <p class="para-w3-agileits">To become a centre of excellence in raising highly skillful, visionary, God-fearing, critical thinkers, ethical and passionate leaders, to function as change-agents in their various spheres of life.</p>
                            <!-- stats -->
                            <div class="stats">
                                <div class="stats_inner">
                                    <div class="col-md-6 col-sm-6 col-xs-6 stat-grids">
                                        <p class="counter-agileits-w3layouts">20</p>
                                        <div class="stats-text-wthree">
                                            <h3>CLASSES</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 stat-grids">
                                        <p class="counter-agileits-w3layouts">87</p>
                                        <div class="stats-text-wthree">
                                            <h3>REVIEWS</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 stat-grids">
                                        <p class="counter-agileits-w3layouts">12</p>
                                        <div class="stats-text-wthree">
                                            <h3> ACTIVITIES</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6 stat-grids">
                                        <p class="counter-agileits-w3layouts">45</p>
                                        <div class="stats-text-wthree">
                                            <h3>Partners</h3>
                                        </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                </div>
                            </div>
                            <!-- //stats -->
        
                        </div>
                    </div>
                    
                </div>
            </div> --}}
        <!--//about-->
    <!-- Testimonials -->
        {{-- <div class="testimonials">
            <div class="container">
                <h3 class="title-txt"><span>T</span>estimonials</h3>
                <div class="col-md-6 testimonials-main">
                    <section class="slider">
                        <div class="flexslider">
                            <ul class="slides">
                                <li>
                                    <div class="inner-testimonials-w3ls">
                                        <img src="images/1.jpg" alt=" " class="img-responsive" />
                                        <div class="testimonial-info-wthree">
                                            <h5>Andy Wovel</h5>
                                            <span>Lorem Ipsum</span>
                                            <p class="para-w3ls">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit lobortis elementum, Quis nostrum exercitationem
                                                ullam corporis suscipit laboriosam. </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner-testimonials-w3ls">
                                        <img src="images/2.jpg" alt=" " class="img-responsive" />
                                        <div class="testimonial-info-wthree">
                                            <h5>Bernard Russo</h5>
                                            <span>Lorem Ipsum</span>
                                            <p class="para-w3ls">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit lobortis elementum, Quis nostrum exercitationem
                                                ullam corporis suscipit laboriosam. </p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="inner-testimonials-w3ls">
                                        <img src="images/3.jpg" alt=" " class="img-responsive" />
                                        <div class="testimonial-info-wthree">
                                            <h5>Alex Merphy & July Mao</h5>
                                            <span>Lorem Ipsum</span>
                                            <p class="para-w3ls">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis hendrerit lobortis elementum, Quis nostrum exercitationem
                                                ullam corporis suscipit laboriosam. </p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </section>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div> --}}
        <!-- //Testimonials -->
        <!-- Events -->
        {{-- <div class="events-section">
            <div class="container">
                <h3 class="title-txt two"><span>O</span>ur News</h3>
                <div class="col-sm-4 live-grids-w3ls">
                    <div class="live-left1">
                        <img src="images/g1.jpg" alt=" " class="img-responsive">
                    </div>
                    <div class="live-info">
                        <ul>
                            <li><span class="fa fa-calendar-o" aria-hidden="true"></span> FEBRUARY 08, 2018</li>
                        </ul>
                        <h4>School funding</h4>
                        <p class="para-1"> Proin ultricies vestibulum velit.Lorem ipsum dolor sit amet.Nam
                            aliquam pretium feugiat.</p>
                    </div>
                    
                </div>
                <div class="col-sm-4 live-grids-w3ls">
                    <div class="live-left2">
                        <img src="images/g2.jpg" alt=" " class="img-responsive">
                    </div>
                    <div class="live-info">
                        <ul>
                            <li><span class="fa fa-calendar-o" aria-hidden="true"></span> FEBRUARY 08, 2018</li>
                        </ul>
                        <h4>School funding</h4>
                        <p class="para-1">Proin ultricies vestibulum velit.Lorem ipsum dolor sit amet.Nam
                            aliquam pretium feugiat.</p>
                    </div>
                    
                    
                </div>
                <div class="col-sm-4 live-grids-w3ls">
                    <div class="live-left3">
                        <img src="images/g6.jpg" alt=" " class="img-responsive">
                    </div>
                    <div class="live-info">
                        <ul>
                            <li><span class="fa fa-calendar-o" aria-hidden="true"></span> FEBRUARY 08, 2018</li>
                        </ul>
                        <h4>School funding</h4>
                        <p class="para-1"> Proin ultricies vestibulum velit.Lorem ipsum dolor sit amet.Nam
                            aliquam pretium feugiat.</p>
                    </div>
                    
                </div>
                <div class="clearfix"> </div>
            </div>
        </div> --}}
        <!-- //Events -->
    {{-- <div class="experience">
        <div class="container">
        <h3 class="title-txt"><span>A </span> Perfect Learning Center</h3>
            <div class="experience-info">
                <div class="col-md-7 exp-matter">
                    <div class="exp-left">
                        <div class="ex-lt">
                            <h6>14 <span>MAR</span></h6>
                        </div>
                        <div class="ex-rt">
                            <h5>How to Start</h5>
                            <p>0 COMMENTS,</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="exp-left">
                        <div class="ex-lt">
                            <h6>16 <span>MAR</span></h6>
                        </div>
                        <div class="ex-rt">
                            <h5>How to Pay</h5>
                            <p> 0 COMMENTS,</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
            
                    <div class="exp-left">
                        <div class="ex-lt">
                            <h6>18 <span>MAR</span></h6>
                        </div>
                        <div class="ex-rt">
                            <h5>Calendar</h5>
                            <p> 0 COMMENTS,</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="exp-left">
                        <div class="ex-lt">
                            <h6>18 <span>MAR</span></h6>
                        </div>
                        <div class="ex-rt">
                            <h5>How to purchase pin</h5>
                            <p> 0 COMMENTS,</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-5 exp-info-right">
                    <div class="ex-top">
                        <h4>LEARNING FACILITIES</h4>
                        <li><span class="fa fa-check" aria-hidden="true"></span>After School</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>After School Fine Arts</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>Athletics</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>Resource Room</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>Dismissal Procedures</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>Dining Hall</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>College Planning Services</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>Summer Camp Programs</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>Two technology centers</li>
                        <li><span class="fa fa-check" aria-hidden="true"></span>Staff Directory</li>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div> --}}
    <!-- footer -->
        <div class="footer">
        <div class="container">
            <div class="f-bg-w3l">
            <div class="col-md-4 w3layouts_footer_grid">
                        <h3>About Us</h3>
                        <p>Regent University College of Science and Technology is an institution with a national agenda and a global focus.</p>
                    </div>
                    <div class="col-md-4 w3layouts_footer_grid hpft">
                        <h3>Contact Us</h3>
                            <ul class="con_inner_text">
                                <li><span class="fa fa-map-marker" aria-hidden="true"></span>Jonkobri Rd, McCarty <label>Accra.</label></li>
                                <li><span class="fa fa-envelope-o" aria-hidden="true"></span> <a href="mailto:info@example.com">regentads@regent.edu.com</a></li>
                                <li><span class="fa fa-phone" aria-hidden="true"></span> +(233) 223 886 6067</li>
                            </ul>
    
                        
                    </div>
                    <div class="col-md-4 w3layouts_footer_grid">
                        <h2>Officials</h2>
                        <p>Admission office portal.</p>
                        {{--  <form action="#" method="post">
                            <input type="email" name="Email" placeholder="Enter your email..." required="">
                            <button class="btn1"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
                            <div class="clearfix"> </div>
                        </form>  --}}
                        <li><a href="#">
                                <span class="fa fa-user" aria-hidden="true"></span>Login above to proceed.
                            </li>
                            
                       
                    </div>
                    <div class="clearfix"> </div>		
                </div>	
                </div>
        </div>
        <p class="copyright">© <php? echo date ('Y') ?> Design by <a href="#">Solomon ISS</a></p>
    
@endsection