
@extends('layouts.admin')


@section('content')

<section class="content">
    <div class="container-fluid">
        @include('notification')
       
        @if(Auth::check() and Auth::user()->role == 'Applicant')

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        WELCOME TO APPLICANT PORTAL 
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <blockquote class="m-b-25">
                        <p>Get updated about the status of you application by simply visiting our portal.</p>
                    </blockquote>
                   
                    <blockquote class="blockquote-reverse">
                            <div class="row clearfix">
                                
                                    {{--  <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                                        <a href="{{ url('/') }}" class="btn btn-primary btn-lg btn-block waves-effect pull-right">Apply <span class="badge">_</span></a>
                                    </div>
                                      --}}
                                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 pull-right">
                                        <a href="{{ url('/profile') }}" class="btn btn-warning btn-lg btn-block waves-effect pull-right">View Profile <span class="badge">_</span></a>
                                    </div>
                                    
                                </div>
                        <footer>Institution with global <cite title="Source Title">Vision</cite></footer>
                    </blockquote>
                </div>
            </div>
        </div>
        
        @endif
        
        
       
        @if(Auth::check() and Auth::user()->role !== 'Applicant')

        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text"> Appliations</div>
                        <div class="number count-to" data-from="0" data-to="{{ count($applications) }}" data-speed="15" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">help</i>
                    </div>
                    <div class="content">
                        <div class="text">Qualified Candiates</div>
                        <div class="number count-to" data-from="0" data-to="{{ $qualifiedapplications }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">forum</i>
                    </div>
                    <div class="content">
                        <div class="text">Endoresed Candidate</div>
                        <div class="number count-to" data-from="0" data-to="{{ $endorsedapplications }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person_add</i>
                    </div>
                    <div class="content">
                        <div class="text">Disqualified</div>
                        <div class="number count-to" data-from="0" data-to="{{ $unqualifiedapplications }}" data-speed="1000" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
        
        <!-- CPU Usage -->
       
        <!-- #END# CPU Usage -->
        <div class="row clearfix">
            <!-- Visitors -->
            
            <!-- #END# Visitors -->
            
            <!-- Answered Tickets -->
            {{-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="body bg-teal">
                        <div class="font-bold m-b--35">ANSWERED TICKETS</div>
                        <ul class="dashboard-stat-list">
                            <li>
                                TODAY
                                <span class="pull-right"><b>12</b> <small>TICKETS</small></span>
                            </li>
                            <li>
                                YESTERDAY
                                <span class="pull-right"><b>15</b> <small>TICKETS</small></span>
                            </li>
                            <li>
                                LAST WEEK
                                <span class="pull-right"><b>90</b> <small>TICKETS</small></span>
                            </li>
                            <li>
                                LAST MONTH
                                <span class="pull-right"><b>342</b> <small>TICKETS</small></span>
                            </li>
                            <li>
                                LAST YEAR
                                <span class="pull-right"><b>4 225</b> <small>TICKETS</small></span>
                            </li>
                            <li>
                                ALL
                                <span class="pull-right"><b>8 752</b> <small>TICKETS</small></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div> --}}
            <!-- #END# Answered Tickets -->
        </div>

        <div class="row clearfix">
            <!-- Application Info -->
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="card">
                    <div class="header">
                        <h2>Recent Applicants</h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">View More</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover dashboard-Application-infos">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Applicant</th>
                                        <th>Program</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @if(count($applications)>0)

                                    <?php $count = 1; ?>
                        
                                    @foreach($applications as $a)
                        
                                    <tr>
                                        <td><?php echo $count;?></td>
                                        <td>{{ $a->fname }} {{ $a->sname }}</td>
                                        <td>{{ $a->program['name'] }}</td>
                                        <td>{{ $a->created_at }}</td>
                                        <td>

                                            @if($a->status == 'Processing')
                                                <span class="label bg-orange">{{ $a->status }}</span>
                                                @elseif($a->status == 'Qualified')
                                                <span class="label bg-blue">{{ $a->status }}</span>
                                                @elseif($a->status == 'Approved')
                                                <span class="label bg-light-blue">{{ $a->status }}</span>
                                                @elseif($a->status == 'Endorse')
                                                <span class="label bg-green">{{ $a->status }}</span>
                                              @else
                                                <span class="label bg-red">{{ $a->status }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    
                                    <?php $count ++; ?>
                                    @endforeach
                                    @else

                                        <tr>
                                            <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> There are no Applicant Request </td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Application Info -->
            <!-- Latest Social Trends -->
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="card">
                    <div class="body bg-cyan">
                        <div class="m-b--35 font-bold">REGENT SOCIAL TRENDS</div>
                        <ul class="dashboard-stat-list">
                            <li>
                                #2016admission
                                <span class="pull-right">
                                    <i class="material-icons">trending_up</i>
                                </span>
                            </li>
                            <li>
                                #2017admission
                                <span class="pull-right">
                                    <i class="material-icons">trending_up</i>
                                </span>
                            </li>
                            <li>#admission2018 </li>
                            <li>#admissions2019 </li>
                            <li>#whyRent</li>
                            <li>
                                #YourbestschoolinAccra
                                <span class="pull-right">
                                    <i class="material-icons">trending_up</i>
                                </span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #END# Latest Social Trends -->
        </div>
    
    @endif
    </div>
</section>

@endsection