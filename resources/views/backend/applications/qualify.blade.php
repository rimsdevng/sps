@extends('layouts.admin')

@section('content')

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                APPLICATION PENDING APPROVAL
                <small>list of candidates awaiting approval</small>
            </h2>
        </div>
      

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            APPLICATIONS
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">More</a></li>
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                            <th>S/N</th>
                                            <th>Full Name</th>
                                            <th>Gender</th>
                                            <th>Program</th>
                                            <th>Date Applied</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                @if(count($applications)>0)

                                    <?php $count = 1; ?>
                    
                                @foreach($applications as $a)
                                    <tr>
                                            <td><?php echo $count;?></td>
                                            <td>{{ $a->fname }} {{ $a->sname }}</td>
                                            <td>{{ $a->gender }}</td>
                                            <td>{{ $a->program->name }}</td>
                                            <td>{{ $a->created_at }}</td>
                                            <td>
    
                                                @if($a->status == 'Processing')
                                                    <span class="label bg-orange">{{ $a->status }}</span>
                                                    @elseif($a->status == 'Qualified')
                                                    <span class="label bg-blue">{{ $a->status }}</span>
                                                    @elseif($a->status == 'UnQualified')
                                                    <span class="label bg-red">{{ $a->status }}</span>
                                                @endif


                                            </td>
                                            <td>
                                                @if($a->status == 'Processing')
                                                
                                                <span><a class="btn btn-success" href="{{url('qualify-new-applicant/'.$a->apid)}}"><i class="ti-pencil-alt color-success"></i> Qualify</a></span>
                                                    @else
                                                    <span><a class="btn btn-warning" href="{{url('unqualify-applicant/'.$a->apid)}}"><i class="ti-pencil-alt color-success"></i> Deny</a></span>

                                                @endif
                                            </td>
                                    </tr>
                                    <?php $count ++; ?>
                                @endforeach
                                @else

                                    <tr>
                                        <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> There are no Applicant Request </td>
                                    </tr>
                                @endif
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
@endsection