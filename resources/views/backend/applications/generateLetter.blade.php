@extends('layouts.print')

@section('content')

<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ADMISSION LETTER</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                                
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                        
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            
                                            <p class="card-inside-title">Mr Jonathan</p>
                                            <p class="card-inside-title">Address : Kasoa</p>

                                            <p class="card-inside-title">Date created_at</p>

                                            <p class="card-inside-title">Dear Name</p>


                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <div class="form-line">
                                                <h3 class="card-inside-title align-center">OFFER OF ADMISSION 2018/2019 ACADEMIC YEAR</h3>
                                            </div>
                                    </div>
                                </div>
                            </div>

                            <h4 class="col-sm-12">
                                    We are happy to offer you admission to study in the University during the 2019/2020 academic year. You have been admitted to study the understated program. 
                            </h4>

                            <div class="col-md-12" style="margin-left:30px">
                                    <p class="card-inside-title">Program Assigned:	Information System Science</p>
                                    <p class="card-inside-title">School: 	FECAS</p>
        
                                    <p class="card-inside-title">Level:	100</p>
        
                                    <p class="card-inside-title">Semester Fee	N/A</p>
        
                                    <p class="card-inside-title">Statutory Payment:	$2,200</p>
                                    <p class="card-inside-title">Orientation Period	Monday, 5th 2019 – Saturday, October 10, 2019 </p>
                                    <p class="card-inside-title">Lecturers 		Starts in Two weeks</p>
                                    <p class="card-inside-title">Conditions of Office	As per the attached</p>
        
                                    <br>
                            </div>
                            <h4 class="col-md-12">
                                    This is a letter of admission for International student subject to verification of qualification. Notwithstanding the above. The university reserve the right to consider for your second or other choice of program where it is not feasible to offer you your choice of programme.
                            </h4>

                            <br>
                            <h4 class="col-md-12">
                                    Congratulations on your admission into this great institution of infinite possibilities. 
                            </h4>

                            <br>
                            <h4 class="col-md-12">
                                    We want you to maximize the use of your time, the opportunities and the resources available to your order to take your rightful place among the wining team. 
                            </h4>

                            
                            
                            
                            
                            
                            <p class="col-md-12"><i> Welcome to Regent-Ghana</i></p>

                            <p class="col-md-12">Yours Sincerely,</p>
                            <br><br>
                            <p class="class="col-md-12"">Nancy Ansah (Ms.)</p>
                            <p class="col-md-12"><strong>Registrar</strong></p>
                          

                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
           
        </div>
    </section>

@endSection
