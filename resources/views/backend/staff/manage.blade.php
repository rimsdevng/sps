@extends('layouts.admin')

@section('content')


<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                    @include('notification')

                MANAGE STAFF USERS
                <small>list of staff in charge of admission system</small>
            </h2>
        </div>
      

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           STAFF USERS 
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">More</a></li>
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                                {{-- dataTable js-exportable --}}
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                            <th>S/N</th>
                                            <th>Title</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Gender</th>
                                            <th>Role</th>
                                            <th>Action</th>


                                    </tr>
                                </thead>

                                <tbody>

                                @if(count($staff)>0)

                                    <?php $count = 1; ?>
                    
                                @foreach($staff as $s)
                                    <tr>
                                            <td><?php echo $count;?></td>
                                            <td>{{ $s->title }}</td>
                                            <td>{{ $s->name }}</td>
                                            <td>{{ $s->email }}</td>
                                            <td>{{ $s->gender }}</td>
                                            <td>{{ $s->role }}</td>
                                            
                                            <td>
                                                
                                               
                                                
                                                <span><a class="btn btn-success" href="{{url('edit-staff/'.$s->uid)}}"><i class="ti-pencil-alt color-success"></i> Edit</a>
                                                </span>

                                                <span><a class="btn btn-danger" href="{{url('delete-program/'.$s->uid)}}"><i class="ti-pencil-alt color-success"></i> Delete</a>
                                                </span>

                                                
                                            </td>
                                            <td>
                                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <i class="material-icons">more_vert</i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="{{ url('make-admin/'.$s->uid) }}">Make Applicant </a></li>
                                                    <li><a href="{{ url('make-registrar/'.$s->uid) }}">Make Registrar</a></li>
                                                    <li><a href="{{ url('make-admissionOfficer/'.$s->uid) }}">Make Admission Officer</a></li>
                                                    <li><a href="{{ url('make-vpacademics/'.$s->uid) }}">Make VP Academics</a></li>
                                                   
                                                </ul>
                                            </td>
                                    </tr>
                                    <?php $count ++; ?>
                                @endforeach
                                @else

                                    <tr>
                                        <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> There are no Applicant Request </td>
                                    </tr>
                                @endif
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
@endsection
