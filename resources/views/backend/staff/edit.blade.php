@extends('layouts.admin')


@section('content')
<style> 
        .pb-8, .py-8{
            padding-top: 0 !important; 
        }

        .select-car{
            width: 100%;
            height: 50px;
            border-radius: 3px;
            border-color: gainsboro;
        }
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
@include('main.top')
</div>

<div class="row mt-5 ml-5 mb-10">
    <div class="col-xl-10 mb-10 mb-xl-0">
            @include('notification')
            <h3>Add Staff Information</h3>
          <div class="card shadow">

            <form method="POST" action="{{ url('post-edit-staff/'.$staff->uid) }}" >
                    {{ csrf_field() }}
                <div class="row">
                <div class="col-md-5 mt-5 ml-5">
                    <div class="form-group">
                    <input type="text" name="title" value="{{ $staff->title }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="title">
                    </div>
                </div>
                 <div class="col-md-5 mt-5 ml-5">
                        <div class="form-group">
                        <input type="text" name="fname" value="{{ $staff->name }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="fname">
                        </div>
                    </div>
                  
                    <div class="col-sm-5 ml-5">
                        <div class="form-group">
                                <select class="form-group select-car" name="gender" default="Gender" type="text">
                                        <option>Gender</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                </select>
                        </div>
                    </div>
        
                  

                </div>
                <div class="row">
                
                
                    <div class="col-md-5 ml-5">
                        <div class="form-group">
                        <input type="email" name="email" value="{{ $staff->email }}" class="form-control form-control-alternative" id="exampleFormControlInput1" placeholder="mail@example.com">
                        </div>
                    </div>
                    
                
                </div>
              
                

                <div class="col-md-10 ml-5 mt-2 mb-3">
                    <button class="btn btn-primary" type="submit">Submit</button>
                    <a href="{{ url('/manage-staff') }}" style="text-decoration:none;" class="btn btn-danger"> Cancel</a>
                </div>   
      </form>
     </div>
  </div>
</div>

@endsection 