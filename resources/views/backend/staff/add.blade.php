@extends('layouts.admin')


@section('content')

<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ADD STAFF</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="card-inside-title">

                                <small>Kindly fill in staff details</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">View</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            @include('notification')
                            <div class="row clearfix">
                                <div class="col-sm-8">
                                <form method="post" action="{{ url('add-staff') }}" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    
                                    <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="title" placeholder="Title" />
                                            </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" placeholder="Full Name" />
                                        </div>
                                    </div>

                                  

                                    <select name="gender" >
                                            <option>Gender</option>
                                            <option>Male</option>
                                            <option>Female</option>
]                                    </select>

                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="phone" placeholder="Phone" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" class="form-control" name="email" placeholder="example@mail.com" />
                                        </div>
                                    </div>

                                    <div class="form-line">
                                        <div class="form-group">
                                            <select name="role" >
                                                    <option>Select Role</option>
                                                    <option>Admin</option>
                                                    <option>VPAcademics</option>
                                                    <option>Registrar</option>
                                                    <option>AdmissionsOffice</option>
                                            </select>
                                        </div>
                                    </div>
                                    

{{--                                     
                                            <div class="demo-radio-button">
                                                    <input name="group1" type="radio" id="radio_1" checked="">
                                                    <label for="radio_1">Radio - 1</label>
                                                    <input name="group1" type="radio" id="radio_2">
                                                    <label for="radio_2">Radio - 2</label>
                                                    <input name="group1" type="radio" class="with-gap" id="radio_3">
                                                    <label for="radio_3">Radio - With Gap</label>
                                                    <input name="group1" type="radio" id="radio_4" class="with-gap">
                                                    <label for="radio_4">Radio - With Gap</label>
                                                    <input name="group2" type="radio" id="radio_5" checked="" disabled="">
                                                    <label for="radio_5">Radio - Disabled</label>
                                                    <input name="group3" type="radio" id="radio_6" class="with-gap" checked="" disabled="">
                                                    <label for="radio_6">Radio - Disabled</label>
                                                </div> --}}
                                    <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect pull-right">Add</button>

                                    <button type="submit" class="btn btn-warning btn-lg m-l-15 waves-effect pull-right">Cancel</button>
                                 </form>
                                </div>
                            </div>

                           
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
          
           
          
            
        
        </div>
    </section>
@endsection
