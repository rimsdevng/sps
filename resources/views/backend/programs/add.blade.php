@extends('layouts.admin')


@section('content')

<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>ADD PROGRAM</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 class="card-inside-title">

                                <small>Kindly fill in details</small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">View</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            @include('notification')
                            <div class="row clearfix">
                                <div class="col-sm-8">
                                <form method="post" action="{{url('/program')}}">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="name" placeholder="Name" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea class="form-control" name="description" id="" cols="30" rows="2"> Description</textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect pull-right">Add</button>
                                 </form>
                                </div>
                            </div>

                           
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
          
           
          
            
        
        </div>
    </section>
@endsection