@extends('layouts.admin')

@section('content')


<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                    @include('notification')

                MANAGE PROGRAMS
                <small>list of programs in the university</small>
            </h2>
        </div>
      

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           PROGRAMS 
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">More</a></li>
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                            <th>S/N</th>
                                            <th>Program</th>
                                            <th>Description</th>
                                            <th>Actions</th>
                                    </tr>
                                </thead>

                                <tbody>

                                @if(count($programs)>0)

                                    <?php $count = 1; ?>
                    
                                @foreach($programs as $p)
                                    <tr>
                                            <td><?php echo $count;?></td>
                                            <td>{{ $p->name }}</td>
                                            <td>{{ $p->description }}</td>
                                            <td>
                                                
                                               
                                                
                                                <span><a class="btn btn-success" href="{{url('edit-program/'.$p->prid)}}"><i class="ti-pencil-alt color-success"></i> Edit</a>
                                                </span>

                                                <span><a class="btn btn-danger" href="{{url('delete-program/'.$p->prid)}}"><i class="ti-pencil-alt color-success"></i> Delete</a>
                                                </span>

                                                
                                            </td>
                                    </tr>
                                    <?php $count ++; ?>
                                @endforeach
                                @else

                                    <tr>
                                        <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> There are no Applicant Request </td>
                                    </tr>
                                @endif
                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
</section>
@endsection
