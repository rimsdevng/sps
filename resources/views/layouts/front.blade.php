    <!DOCTYPE html>
    <html lang="en">
    
    <head>
        <title>Applicant Applicatin portal</title>
        <!-- Meta Tags -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Applicant application portal, Regent University Ghana, Regent, Ghana" />
        <script type="application/x-javascript">
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);
    
            function hideURLbar() {
                window.scrollTo(0, 1);
            }
        </script>
        <!-- // Meta Tags -->
        <link href="{{ url('frontend/css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('frontend/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" media="all">
        <link rel="stylesheet" href="{{ url('frontend/css/flexslider.css') }}" type="text/css" media="screen" property="" />
        <!--testimonial flexslider-->
        <link href="{{ url('frontend/css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
        <!--fonts-->
        <link href="//fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Raleway:300,400,500,600,800" rel="stylesheet">
        <!--//fonts-->
    
    </head>
    
    <body>
        {{-- the header was here --}}
        
        @yield('content')

        <!-- //footer -->
    
    
    
        <!-- Required Scripts -->
        <!-- Common Js -->
        <script type="text/javascript" src="{{ url('frontend/js/jquery-2.2.3.min.js') }}"></script>
        <!--// Common Js -->
        <!--search-bar-agileits-->
        <script src="{{ url('frontend/js/main.js') }}"></script>
        <!--//search-bar-agileits-->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="{{ url('frontend/js/move-top.js') }}"></script>
        <script type="text/javascript" src="{{ url('frontend/js/easing.js') }}"></script>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();
                    $('html,body').animate({
                        scrollTop: $(this.hash).offset().top
                    }, 1000);
                });
            });
        </script>
        <!-- start-smoth-scrolling -->
    
        <!-- Banner Responsive slider -->
        <script src="{{ url('frontend/js/responsiveslides.min.js') }}"></script>
        <script>
            // You can also use "$(window).load(function() {"
            $(function () {
                // Slideshow 3
                $("#slider3").responsiveSlides({
                    auto: true,
                    pager: false,
                    nav: true,
                    speed: 500,
                    namespace: "callbacks",
                    before: function () {
                        $('.events').append("<li>before event fired.</li>");
                    },
                    after: function () {
                        $('.events').append("<li>after event fired.</li>");
                    }
                });
    
            });
        </script>
        <!-- // Banner Responsive slider -->
    
        <!-- flexSlider -->
        <script defer src="{{ url('frontend/js/jquery.flexslider.js') }}"></script>
        <script type="text/javascript">
            $(window).load(function () {
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });
        </script>
        <!-- //flexSlider -->
    
        <!-- stats -->
        <script src="{{ url('frontend/js/jquery.waypoints.min.js') }}"></script>
        <script src="{{ url('frontend/js/jquery.countup.js') }}"></script>
        <script>
            $('.counter-agileits-w3layouts').countUp();
        </script>
        <!-- //stats -->
        <!-- smooth scrolling -->
        <script type="text/javascript">
            $(document).ready(function () {
                /*
                    var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear' 
                    };
                */
                $().UItoTop({
                    easingType: 'easeOutQuart'
                });
            });
        </script>
        <a href="#home" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        <!-- //smooth scrolling -->
    
     <script src="{{ url('frontend/js/bootstrap.js') }}"></script>
    
    
        <!--// Required Scripts -->
    </body>
    
    </html>