
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Admission Portal</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ url('frontend/images/logo.png') }}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">


    <!-- Bootstrap Core Css -->
    <link href="{{ url('backend/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ url('backend/plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ url('backend/plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="{{ url('backend/plugins/dropzone/dropzone.css') }}" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="{{ url('backend/plugins/multi-select/css/multi-select.css') }}" rel="stylesheet">

    <!-- JQuery DataTable Css -->
    <link href="{{ url('../backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- Custom Css -->
    <link href="{{ url('backend/css/style.css') }}" rel="stylesheet">
</head>

<body class="login-page">
   
   @yield('content')

    <!-- Jquery Core Js -->
    <script src="{{ url('backend/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ url('backend/plugins/bootstrap/js/bootstrap.js') }}"></script>

   
    <!-- Waves Effect Plugin Js -->
    <script src="{{ url('backend/plugins/node-waves/waves.js') }}"></script>




    <!-- Validation Plugin Js -->
    <script src="{{ url('backend/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ url('backend/js/admin.js') }}"></script>

    <script src="{{ url('backend/js/pages/examples/sign-in.js') }}"></script>
</body>

</html>