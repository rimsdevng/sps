@extends('layouts.front')


@section('content')

<!-- typography -->
<div class="typo">
    <div class="container">
        <h3 class="title-txt"><span>Pay </span>To Complete Application</h3>
        
    @include('notification')
        <div class="grid_3 grid_5 w3l">
            <h3>Home-> Start Application-> Pay-> Complete</h3>
            <div class="tab-content">
                <div class="tab-pane active" id="domprogress">
                    
                    <div class="progress">    
                        <div class="progress-bar progress-bar-info" style="width: 90%"></div>
                    </div>
                    
                    
                    <div class="submit1">
                            
                            <a href="{{ url('/') }}" style="padding-left:40px; padding-right:40px; padding-top:10px; padding-bottom:10px;" class="btn btn-success pull-right">Complete Application</a>
                    </div>
                </div>
            </div>
        </div>
        
        
      
        
        
        
       
        
        
        
    </div>
</div>
<!-- //typography -->

@endsection 