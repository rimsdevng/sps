<div class="input-group input-group-lg w3_w3layouts">
        <span class="input-group-addon" id="sizing-addon1"></span>
        <input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon1">
</div>

<div class="input-group input-group-lg w3_w3layouts">
        <span class="input-group-addon" id="sizing-addon1"></span>
        <input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon1">
</div>

<div class="input-group input-group-lg w3_w3layouts">
    <input type="text" class="form-control" placeholder="Recipient's username" aria-describedby="basic-addon2">
    <span class="input-group-addon" id="basic-addon2">@example.com</span>
</div>

<div class="input-group w3_w3layouts">
    <span class="input-group-addon" id="sizing-addon2">@</span>
    <input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon2">
</div>
<div class="input-group input-group-sm w3_w3layouts">
    <span class="input-group-addon" id="sizing-addon3">@</span>
    <input type="text" class="form-control" placeholder="Username" aria-describedby="sizing-addon3">
</div>


<div class="row">
    <div class="col-lg-6 in-gp-tl">
        <div class="input-group">
            <span class="input-group-addon">
                <input type="checkbox" aria-label="...">
            </span>
            <input type="text" class="form-control" aria-label="...">
        </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
    <div class="col-lg-6 in-gp-tb">
        <div class="input-group">
            <span class="input-group-addon">
                <input type="radio" aria-label="...">
            </span>
            <input type="text" class="form-control" aria-label="...">
        </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
</div>

<!-- /.row -->
<div class="row">
    <div class="col-lg-6 in-gp-tl">
        <div class="input-group">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
            </span>
            <input type="text" class="form-control" placeholder="Search for...">
        </div>
        <!-- /input-group -->
    </div>
    
    <!-- /.col-lg-6 -->
    <div class="col-lg-6 in-gp-tb">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button">Go!</button>
            </span>
        </div><!-- /input-group -->
    </div>
    <!-- /.col-lg-6 -->
</div>


<!-- /.row -->
<div class="row">
    <div class="col-lg-6 in-gp-tl">
        <div class="input-group">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div><!-- /btn-group -->
            <input type="text" class="form-control" aria-label="...">
        </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
    <div class="col-lg-6 in-gp-tb">
        <div class="input-group">
            <input type="text" class="form-control form-span" aria-label="...">
            {{-- <div class="input-group-btn">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                </ul>
            </div> --}}
            <!-- /btn-group -->
        </div><!-- /input-group -->
    </div><!-- /.col-lg-6 -->
</div><!-- /.row -->