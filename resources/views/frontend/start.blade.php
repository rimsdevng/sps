@extends('layouts.front')


@section('content')

@include('navs.defaultNavs')
<style>
    .form-span{
        width: 250% !important;
    }

    .contact-form input[type="text"], .contact-form input[type="email"], textarea {
        margin: .5em 0;
        padding: .7em 1em;
        font-size: .9em;
        width: 100%;
        background: transparent;
        border: 1px solid #ccc;
        border-radius: 5px;
        color: #555;
        letter-spacing: 2px;
    }
</style>

<div class="typo">
    <div class="container">

        	<div class="grid_3 grid_5 agileinfo">
                  
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Application</li>
                    </ol>
                </div>
                @include('notification')
                <h3 class="bars">Kindly Fill in your details below: </h3>
                <form method="post" action="{{url('post-application')}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                    
                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <input type="text" name="sname" class="form-control" placeholder="SurName" aria-describedby="sizing-addon1" required>
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <input type="text" class="form-control" placeholder="Forename" name="fname" aria-describedby="sizing-addon1" required>
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <input type="text" class="form-control" placeholder="Othername" name="oname" aria-describedby="sizing-addon1">
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <input type="date" class="form-control" placeholder="Date of birth" name="dob" aria-describedby="sizing-addon1" required>
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <input type="text" class="form-control" placeholder="Phone Number" name="phone" aria-describedby="sizing-addon1" required>
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <input type="email" class="form-control" placeholder="Email" name="email" aria-describedby="sizing-addon1" required>
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <select class="form-control" name="gender" id="gender">
                            <option>--Select Gender--</option>
                            <option>Male</option>
                            <option>Female</option>
                        </select>
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <select class="form-control" name="prid" class="form-control"id="program">
                            @foreach($program as $p)
                            <option value="{{$p->prid}}">{{$p->name}}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        <input type="text" class="form-control" placeholder="Nationality" name="nationality" aria-describedby="sizing-addon1">
                    </div>

                    {{-- Add more regions --}}
                    <div class="input-group input-group-lg w3_w3layouts">
                        <span class="input-group-addon" id="sizing-addon1"></span>
                        {{-- <select class="form-control" name="region" id="region">
                            <option>--Select Region--</option>
                            <option>Accra</option>
                            <option>Kumasi</option>
                            <option>Cape Coast</option>
                            <option>Others</option>
                        </select> --}}
                        <input type="text" class="form-control" placeholder="Region/State" name="region" required>

                    </div>
                    <hr>
                    <div class="col-lg-6 in-gp-tl">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <label for="Friends">Friends</label>
                            <input type="checkbox" value="Friends"  name="hear_of_regent" aria-label="...">
                        </span>		
                        
                        <span class="input-group-addon">
                            <label for="Advertisement">Advertisement</label>
                            <input type="checkbox" value="Advertisement" name="hear_of_regent"   aria-label="...">
                        </span>

                        <span class="input-group-addon">
                            <label for="Others">Others</label>
                            <input type="checkbox" value="Others" name="hear_of_regent"  aria-label="...">
                        </span>
                    </div>        
                    </div>

                    <hr>
                    
                    <div class="col-lg-7 in-gp-tl">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1">
                        <label for="">Photo</label>
                        <input type="file" class="form-control"name="image">
                        </span>
                    </div>
                    </div>
                    <hr>
                    
                    <div class="form-group">
                        <textarea name="address" id="" cols="30" rows="3" required>Address</textarea>
                    </div>

                    <hr>

                    <div class="form-group">
                        <textarea name="comment" id="" cols="30" rows="3">Comment</textarea>
                    </div>

                    <div class="submit1">
                        <input type="submit" style="padding-left:40px; padding-right:40px; padding-top:10px; padding-bottom:10px;" class="btn btn-success pull-right" value="Submit">
                    </div>
{{-- 
language_spoken
religion_denominatio 
--}}
        </form>
    </div>
</div>

@endsection 