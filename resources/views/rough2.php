<?php

namespace App\Http\Controllers;

use App\Application;
use App\Faq;
use App\Gallery;
use App\Gallery_images;
use App\Policy;
use App\User;
use App\Vacancy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
//        $this->middleware('isApplicant');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$applications = Application::all();
    	$policies = Policy::all();
    	$faqs = Faq::all();
    	$galleries = Gallery::all();

        return view('backend.dashboard',[
        	'policies' => $policies,
	        'applications' => $applications,
	        'faqs' => $faqs,
	        'galleries' => $galleries
        ]);
    }


	public function testLay(  ) {
    	return view('layouts.admin');
    }


	public function getVacancy(  ) {
    	$vacancies = Vacancy::all();
		return view('backend.employment.add',[
			'vacancies' => $vacancies
		]);
    }


	public function postVacancy(Request $request  ) {

    	$vacancy = new Vacancy();
    	$vacancy->title = $request->input('title');
    	$vacancy->job_hours = $request->input('job_hours');
    	$vacancy->job_type = $request->input('job_type');
		$vacancy->short_description = $request->input('short_description');
		$vacancy->description = $request->input('description');
		$vacancy->qualification = $request->input('qualification');
		$vacancy->how_to_apply = $request->input('how_to_apply');
		$vacancy->status = 'open';
//		return $vacancy;

		$status = $vacancy->save();
		if ($status){
			session()->flash('success','vacancy posted');
		}else{
			session()->flash('error','Sorry,There was an error');
		}
		return redirect()->back();
    }

	public function availableVacancies(  ) {

    	$vacancies = Vacancy::where('status','open')->orderBy('created_at','Desc')->paginate(5);
//    	return $vacancies;
    	return view('backend.employment.manage',[
    		'vacancies' => $vacancies
	    ]);

    }


	public function logout() {
		auth()->logout();
		session()->flash('success',"You have been logged out.");
		return redirect('/');
    }



    //Faq

	public function addFaq() {
		return view('backend.faq.add');
	}

	public function postFaq(Request $request  ) {

//    	return $request->all();
    	$faq = new Faq();
    	$faq->question = $request->input('question');
    	$faq->answer = $request->input('answer');

		$status = $faq->save();

    	if ($status){
    		session()->flash('success','Faq added');
	    }else{
		    session()->flash('error','Something Horrible went wrong, Try again');

	    }

	    return redirect()->back();
	}


	public function faqs() {

		if (Input::has('term')){
			$term = Input::get('term');
			$faqs = Faq::where('question','like',"%$term%")->paginate(5);

		}else{
			$faqs = Faq::orderBy('created_at','desc')->paginate(10);

		}






//    	if (Input::has('term')){
//    		$term = Input::get('term');
//    		$faqs  = Faq::where('question','like',"%$term%")->get();
//	    }else{
//		    $faqs = Faq::paginate(10);
//
//	    }

		return view('backend.faq.manage',[
			'faqs' => $faqs
		]);
	}



	public function faqDetail( $fid ) {

		$faq = Faq::findorfail($fid);
		return view('backend.faq.detail',[
			'faq' => $faq
		]);
	}


	public function editFaq($fid) {
		$faq = Faq::find($fid);
		return view('backend.faq.edit',[
			'faq' => $faq
		]);
	}


	public function updateFaq(Request $request,$fid) {
		$policy = Faq::findorfail($fid);
		$update = $policy->update($request->all());
		if ($update){
			session()->flash('success','Faq Updated');
		}else{
			session()->flash('error','sorry, something went wrong');
		}
		return redirect('get-faqs');

	}







	public function getApplications(Request $request) {

    	if (Input::has('name')){


    		$term =  Input::get('name');

    		if ($term = ''){
			    $applications = Application::where('status','pending')->paginate(15);

		    }
		    $applications = Application::where('forenames','like',$term)->orderBy('created_at','desc')->paginate(15);
	    }else{
		    $applications = Application::where('status','pending')->paginate(15);

	    }
    	return view('backend.application.manage',[
    		'applications' => $applications
	    ]);


	}


	public function applicationDetail( $aid  ) {
    	$application = Application::findorfail($aid);
    	return view('backend.application.detail',[
    		'application' =>$application
    		]);


	}

	public function admitStudent($apid) {
    	$student = Application::where('apid',$apid)->first();
//    	return $student;
    	if ($student->payment == "paid"){
//    		return $student;
    		$student->status = "admitted";
		    $status = $student->save();
    		if ($status){
    			session()->flash('success','student Admitted');
    			return redirect('view-applications');
		    }else{
    			session()->flash('error','something went wrong');
		    }
	    }else{
		    session()->flash('error','Application fee not yet paid, student cant be admitted');
		    return redirect()->back();
	    }

	}


	public function getGallery(  ) {

    	$galleries = Gallery::orderBy('created_at','desc')->paginate(20);
		return view('backend.photos.manageGallery',[
			'galleries' => $galleries
		]);
	}



	public function addGallery(  ) {
    	return view('backend.photos.addGallery');
	}


	public function postGallery(Request $request) {
//    	return $request->all();

		if($request->hasFile('image')){
			$filename = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('uploads',Carbon::now()->timestamp . $filename);
			$imageUrl = url('uploads/' . Carbon::now()->timestamp. $filename);
		} else {
			$imageUrl = url('default-image.png');
		}


		$gallery = new Gallery();
    	$gallery->caption = $request->input('caption');
    	$gallery->cover = $imageUrl;
    	$gallery->name = $request->input('name');
    	$status = $gallery->save();
    	if ($status){
    		session()->flash('success','Gallery Added');
	    }else{
    		session()->flash('error','Something Went Wrong');
	    }
	    return redirect('/gallery');

	}


	public function galleryDetail(Request $request, $gid ) {
    	$gallery = Gallery::findorfail($gid);
    	$images =  Gallery_images::where('gid',$gid)->orderBy('created_at','desc')->paginate(20);
//    	return $images;
//		return $gallery->gid;
    	return view('backend.photos.galleryDetail',[
    		'gallery' => $gallery,
		    'images' => $images
	    ]);

	}





	public function addImages($gid){
    	$gallery = Gallery::findorfail($gid);
    	return view('backend.photos.addPhoto',[
    		'gallery' => $gallery
	    ]);
	}




//
//	public function postAddImages(Request $request, $gid ) {
//        	$images = $request->input('images');
//		if($request->hasFile('images')){
//			foreach ( $request->file( 'images' ) as $item ) {
//				$rand = Str::random( 5 );
//				$inputFileName = $item->getClientOriginalName();
//				$item->move( "GalleryImages", $rand . $inputFileName );
//
//
//				$image = new  Gallery_images();
//				$image->gid = $gid;
//				$image->image = url( 'GalleryImages/' . $rand . $inputFileName );
//				$image->save();
//			}
//		}
//	}



	public function postAddImages(Request $request,$gid ) {
//		$status = false;
//		return $request->all();

		if($request->hasFile('images')) {
			foreach ( $request->file( 'images' ) as $item ) {
				$rand           = Str::random(5);
				$inputFileName = $item->getClientOriginalName();
				$item->move( "galleryUploads", $rand . $inputFileName );

				$images      = new Gallery_images();
				$images->image = url( 'galleryUploads/'.$rand.$inputFileName );
				$images->gid = $gid;
				$status     = $images->save();
			}

			if($status)
				$request->session()->flash('success','images added');
			else
				$request->session()->flash('error','Sorry an error occurred');


		}


		return redirect('get-gallery');
	}


	public function deleteImages(Request $request, $giid) {
		$image = Gallery_images::destroy($giid);
		if ($image){
			session()->flash('success','Image deleted Sucessful');
		}else{
			session()->flash('error','Sorry something went wrong');
		}
		return redirect()->back();
	}



















	public function getPolicy(  ) {
		return view('backend.policy.add');
	}


	public function postPolicy(Request $request ) {

    	$policy = new Policy();
    	$policy->title = $request->input('title');
    	$policy->content =  $request->input('content');
    	$status = $policy->save();

    	if ($status){
    		session()->flash('success','Policy Added');
	    }else{
    		session()->flash('error','Something Went Wrong');
	    }

    	return redirect()->back();

	}

	public function managePolicy(Request $request) {

    	if (Input::has('term')){
    		$term = Input::get('term');
		    $policies = Policy::where('title','like',"%$term%")->paginate(6);

	    }else{
		    $policies = Policy::orderBy('created_at','desc')->paginate(10);
	    }
//    	return $policies;
    	return view('backend.policy.manage',[
    		'policies' => $policies
	    ]);
	}


	public function policyDetail( $polid ) {

    	$policy = Policy::findorfail($polid);
    	return view('backend.policy.detail',[
    		'policy' => $policy
	    ]);
	}


	public function editPolicy($polid) {
    	$policy = Policy::find($polid);
    	return view('backend.policy.edit',[
    		'policy' => $policy
	    ]);
	}


	public function updatePolicy(Request $request,$polid) {
    	$policy = Policy::findorfail($polid);
    	$update = $policy->update($request->all());
    	if ($update){
    		session()->flash('success','Policy Updated');
	    }else{
    		session()->flash('error','sorry, something went wrong');
	    }
	    return redirect('manage-policy');

	}


	public function deletePolicy( $polid ) {

    	$policy = Policy::destroy($polid);
    	if ($policy){
    		session()->flash('success','Deleted successfully');
	    }else{
		    session()->flash('error','Sorry something went wrong');

	    }
	    return redirect()->back();



	}


	public function adduser(  ) {
		return view('backend.users.add');

		}

	public function postUser(Request $request  ) {

    	$user = new User();
    	$user->name = $request->input('name');
    	$user->email = $request->input('email');
    	$user->role = $request->input('role');
    	$user->password = bcrypt('Hendon123');
    	$user->state = 'active';
    	$status = $user->save();

    	if ($status){
    		session()->flash('success','User Added');
	    }else{
    		session()->flash('error', 'Something went Wrong');
	    }
	    return redirect()->back();

	}


	public function users(Request $request  ) {
    	if (Input::has('term')){

    		$term = Input::get('term');
		    $users = User::where('name','like',"%$term%")->where('role','Staff')->paginate(5);
//		    return $users;
	    }else{
//		    $users = User::where('role','Staff')->get();
		    $users = User::where('role','Staff')->orWhere('role','Admin')->paginate(5);

	    }
		return view('backend.users.manage',[
 	 		'users' => $users
			]);
	}




	public function suspendUser( Request $request,$uid) {
    	$user = User::findorfail($uid);
    	$user->state = 'in_active';
    	$status =  $user->save();
    	if ($status){
    		session()->flash('success','User Account Suspended');
	    }else{
    		session()->flash('error','Something Went Wrong');
	    }
	    return redirect()->back();
	}

	public function activateUser( Request $request,$uid) {
		$user = User::findorfail($uid);
		$user->state = 'active';
		$status =  $user->save();
		if ($status){
			session()->flash('success','User Account Activated');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
	}


	public function deleteUser( Request $request,$uid) {
		$user = User::destroy($uid);
//		$status =  $user->save();
		if ($user){
			session()->flash('success','User Account Deleted');
		}else{
			session()->flash('error','Something Went Wrong');
		}
		return redirect()->back();
	}

	
	public function changePassword(  ) {

    	return view('backend.users.changepassword');
		
	}

	public function postChangePassword(Request $request ) {
    	$id = Auth::User()->uid;
    	$user = User::findorfail($id);

    	$old = $request->input('old');
    	$new = $request->input('new');

    	if (password_verify($old,$user->password)){
    		$user->password = bcrypt($new);
    		$user->save();

    		session()->flash('success','New password Set');
	    }else{
    		session()->flash('error','password does not match');
	    }

    	return redirect()->back();




	}













}
