
@extends('layouts.admin')


@section('content')

<section class="content">
    <div class="container-fluid">
        @include('notification')
       

        <div class="block-header">
                <h2>Profile</h2>
        </div>

        <div class="row clearfix">
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="card">
                            <div class="header">
                                <h2>Application Information</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="{{ url('/') }}">Back</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="table-responsive">
                                    <table class="table table-hover dashboard-Application-infos">
                                        <thead>
                                            <tr>
                                                <th>Applicant</th>
                                                <th>Email</th>
                                                <th>Date</th>
                                                <th>Application Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                          
                                
                                            <tr>
                                                <td>{{Auth::user()->name}}</td>
                                                
                                                <td>{{Auth::user()->email}}</td>
                                                
                                                <td>{{Auth::user()->created_at}}</td>
                                                <td>
        
                                                    {{--  @if($a->status == 'Processing')
                                                        <span class="label bg-orange">{{ $a->status }}</span>
                                                        @elseif($a->status == 'Qualified')
                                                        <span class="label bg-blue">{{ $a->status }}</span>
                                                        @elseif($a->status == 'Approved')
                                                        <span class="label bg-light-blue">{{ $a->status }}</span>
                                                        @elseif($a->status == 'Endorse')
                                                        <span class="label bg-green">{{ $a->status }}</span>
                                                      @else
                                                        <span class="label bg-red">{{ $a->status }}</span>
                                                    @endif  --}}
                                                </td>

                                                <td>
                                                        <td>{{Auth::user()->apid}}</td>

                                                </td>
                                            </tr>
                                            
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    
       
       
    </div>
</section>

@endsection