<div class="slider">
    <div class="callbacks_container">
        <ul class="rslides" id="slider3">
            <li>
                <div class="slider-img1">
                    <div class="container">
                        <div class="slider_banner_info_w3ls">
                            <h4>Apply & <span>Learn</span></h4>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-img2">
                    <div class="container">
                        <div class="slider_banner_info_w3ls">
                            <h4>Play, Explore & <span>Learn</span></h4>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="slider-img3">
                    <div class="container">
                        <div class="slider_banner_info_w3ls">
                            <h4>Play, Explore & <span>Learn</span></h4>
                        </div>
                    </div>
                </div>
            </li>

        </ul>
    </div>
</div>
<div class="clearfix"> </div>
<!-- //Modal1 -->