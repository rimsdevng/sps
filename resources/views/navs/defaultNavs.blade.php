<!--Header-->
        <div class="header">
        <div class="top">
                        <div class="container">
                            
                                <div class="col-md-9 top-left">
                                    <ul>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i> Jonkobri Rd, Accra</li>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i> +(233) 223 886 6067</li>
                                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="mailto:info@example.com">regentads@regent.edu.com</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3 top-middle">
                                    <ul>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                                
                                <div class="clearfix"></div>
                            
                        </div>
                    </div>
            <!--top-bar-w3layouts-->
                    @include('navs.mainNav')
            <!--//top-bar-w3layouts-->
            <!--Slider-->
            
            <!--//Slider-->
        </div>
        <!--//Header-->
