<div class="top-bar-w3layouts">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <h1></h1><a href="{{ url('/') }}">
                        <img src="{{ url('frontend/images/logo.png') }}" alt="">
                    </a>
                </div>
                <!-- navbar-header -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ url('/') }}" class="active">Home</a></li>
                        <li><a href="{{ url('/') }}/#about">About</a></li>
                        
                        @if(auth()->check())
                        <li><a href="{{ url('start-application') }}">Apply</a></li>
                        @endif
                        

                        @if (Route::has('login'))  
                            @auth
                                
                                <li><a href="{{ url('/home') }}">Profile</a></li>

                                <li>
                                        <a href="{{ url('/logout') }}">
                                            <span>Logout</span>
                                        </a>
                                </li>
                            @else
                                
                                <li><a href="{{ route('login') }}">Login</a></li>
        
                                 @if (Route::has('register'))
                                    
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                @endif 
                            @endauth
                    @endif 
                        {{-- <li><a href="#">Contact</a></li> --}}
                    </ul>

                </div>

                <div class="search-bar-agileits">
                    <div class="cd-main-header">
                        <ul class="cd-header-buttons">
                            <li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
                        </ul>
                        <!-- cd-header-buttons -->
                    </div>
                    <div id="cd-search" class="cd-search">
                        <form action="#" method="post">
                            <input name="Search" type="search" placeholder="Click enter after typing...">
                        </form>
                    </div>
                </div>


                <div class="clearfix"> </div>
            </nav>
        </div>

    </div>